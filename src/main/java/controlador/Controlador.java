/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author emoru
 */

import modelo.Cotizacion;
import vista.dlgCotizacion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {
    private Cotizacion cotizacion;
    private dlgCotizacion vista;

    public Controlador(Cotizacion cotizacion, dlgCotizacion vista) {
        this.cotizacion = cotizacion;
        this.vista = vista;
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }

    public void iniciarVista() {
        vista.setTitle(":: Productos ::");
        vista.setSize(560, 650);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnLimpiar) {
            vista.txtDescripcion.setText("");
            vista.txtNumCotizacion.setText("");
            vista.txtPagoInicial.setText("");
            vista.txtPagoMensual.setText("");
            vista.txtPorcentajePagoInicial.setText("");
            vista.txtPrecio.setText("");
            vista.txtTotalAFin.setText("");
            vista.cboPlazo.setEnabled(false);
        }

        if (e.getSource() == vista.btnNuevo) {
            vista.txtNumCotizacion.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPorcentajePagoInicial.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.cboPlazo.setEnabled(true);
        }
        if (e.getSource() == vista.btnGuardar) {
            cotizacion.setNumDeCotizacion(vista.txtNumCotizacion.getText());
            cotizacion.setDescripcion(vista.txtDescripcion.getText());
            try {
                float x = (Float.parseFloat(vista.txtPorcentajePagoInicial.getText()));
                if (x <= 100) {
                    cotizacion.setPorcentajeDePago(x);
                } else {
                    JOptionPane.showMessageDialog(vista, "El porcentaje no puede ser mayor a 100");
                    return;
                }
                cotizacion.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                cotizacion.setPlazo(Integer.parseInt(vista.cboPlazo.getSelectedItem().toString()));
                JOptionPane.showMessageDialog(vista, "Se agrego Exitosamente");
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex2.getMessage());
            }
            vista.txtPagoMensual.setText(String.valueOf(cotizacion.cacularPagoMensual()));
            vista.txtTotalAFin.setText(String.valueOf(cotizacion.cacularPagoTotal()));
            vista.txtPagoInicial.setText(String.valueOf(cotizacion.cacularPagoIncial()));
        }

        if (e.getSource() == vista.btnMostrar) {
            vista.txtDescripcion.setText(cotizacion.getDescripcion());
            vista.txtNumCotizacion.setText(cotizacion.getNumDeCotizacion());
            vista.txtPrecio.setText(String.valueOf(cotizacion.getPrecio()));
            vista.txtPorcentajePagoInicial.setText(String.valueOf(cotizacion.getporcentajeDePago()));
            vista.cboPlazo.setSelectedItem(String.valueOf(cotizacion.getPlazo()));
            vista.txtPagoMensual.setText(String.valueOf(cotizacion.cacularPagoMensual()));
            vista.txtTotalAFin.setText(String.valueOf(cotizacion.cacularPagoTotal()));
            vista.txtPagoInicial.setText(String.valueOf(cotizacion.cacularPagoIncial()));
        }

        if (e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "Seguro que quieres salir",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }

        if (e.getSource() == vista.btnCancelar) {
            vista.txtDescripcion.setText("");
            vista.txtNumCotizacion.setText("");
            vista.txtPagoInicial.setText("");
            vista.txtPagoMensual.setText("");
            vista.txtPorcentajePagoInicial.setText("");
            vista.txtPrecio.setText("");
            vista.txtTotalAFin.setText("");
            vista.txtNumCotizacion.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPorcentajePagoInicial.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.cboPlazo.setEnabled(false);
            
        }
    }


}
