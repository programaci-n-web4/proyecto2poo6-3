/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author emoru
 */

public class Cotizacion {
    private String NumDeCotizacion;
    private String descripcion;
    private float precio;
    private float PorcentajeDePago;
    private int plazo;
    
    public Cotizacion() {
        
    }
    
    public Cotizacion(String NumDeCotizacion, String descripcion, float precio, float PorcentajeDePago, int plazo) {
        this.NumDeCotizacion = NumDeCotizacion;
        this.descripcion = descripcion;
        this.precio = precio;
        this.PorcentajeDePago = PorcentajeDePago;
        this.plazo = plazo;
    }
    
    public Cotizacion(Cotizacion cotizacion) {
        this.NumDeCotizacion = cotizacion.NumDeCotizacion;
        this.descripcion = cotizacion.descripcion;
        this.precio = cotizacion.precio;
        this.PorcentajeDePago = cotizacion.PorcentajeDePago;
        this.plazo = cotizacion.plazo;
    }
    
    public String getNumDeCotizacion() {
        return this.NumDeCotizacion;
    }
    
    public void setNumDeCotizacion(String NumDeCotizacion) {
        this.NumDeCotizacion = NumDeCotizacion;
    }
    
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public float getPrecio() {
        return this.precio;
    }
    
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public float getporcentajeDePago() {
        return this.PorcentajeDePago;
    }
    
    public void setPorcentajeDePago(float PorcentajeDePago) {
        this.PorcentajeDePago = PorcentajeDePago;
    }
    
    public int getPlazo() {
        return this.plazo;
    }
    
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    public float cacularPagoIncial() {
        return this.precio * (this.PorcentajeDePago / 100);
    }
    
    public float cacularPagoTotal() {
        return this.precio - cacularPagoIncial();
    }
    
    public float cacularPagoMensual() {
        return cacularPagoTotal() / getPlazo();
    }
}

