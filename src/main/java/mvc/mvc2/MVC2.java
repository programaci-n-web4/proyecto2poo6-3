/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package mvc.mvc2;

import controlador.Controlador;
import javax.swing.JFrame;
import modelo.Cotizacion;
import vista.dlgCotizacion;

/**
 *
 * @author emoru
 */
public class MVC2 {

        public static void main(String[] args) {
        Cotizacion cotizacion = new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(), true);

        Controlador controlador = new Controlador(cotizacion, vista);
        controlador.iniciarVista();
        
    }
}